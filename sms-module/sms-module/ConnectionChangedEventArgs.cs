﻿using System;

namespace Vital
{
    public class ConnectionChangedEventArgs : EventArgs
    {
        public ConnectionChangedEventArgs(DateTime time)
        {
            Time = time;
        }

        public DateTime Time { get; set; }
    }
}