﻿

using System.Text;

namespace Constant.Utility
{
    public static class GenericUtility
    {
        public static string ToHexString(this byte[] data)
        {
            var sb = new StringBuilder();
            foreach (var b in data)
            {
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }
    }
}
