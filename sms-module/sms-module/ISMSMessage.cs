﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vital
{
    public enum MessageFormat { Pdu, Text }

    public class SMSTextMessage : Vital.ISMSMessage
    {
        public SMSTextMessage()
        {

        }

        public SMSTextMessage(string content, string to)
        {
            Content = content;
            To = to;
        }

        public string Content { get; set; }

        public MessageFormat Format
        {
            get
            {
                return MessageFormat.Text;
            }
        }

        public string To { get; set; }

        public string GetSendData()
        {
            return To;
        }

        public string ToMessage()
        {
            // (char)26: crtl + z
            return Content + char.ConvertFromUtf32(26);
        }
    }

    public interface ISMSMessage
    {
        MessageFormat Format { get; }

        string To { get; set; }

        string Content { get; set; }

        string GetSendData();

        string ToMessage();
    }


}
