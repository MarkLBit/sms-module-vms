﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;

namespace Vital
{
    /// <summary>
    /// Minimum supported: Windows Vista, Windows Server 2003
    /// </summary>
    public class PNPSerialPortWatcher
    {
        private const string PortGuid = "{4d36e978-e325-11ce-bfc1-08002be10318}";
        private readonly ManagementEventWatcher _creationWatcher;
        private readonly ManagementEventWatcher _deletionWatcher;
        private readonly List<string> _portList = new List<string>();


        public PNPSerialPortWatcher(string port)
        {
            //var query = "SELECT * FROM __InstanceModificationEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_PnPEntity' AND PreviousInstance.HardwareID != TargetInstance.HardwareID";

            var creationQuery = string.Format("SELECT * FROM __InstanceCreationEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_PnPEntity' AND TargetInstance.ClassGuid = '{0}'", PortGuid);
            _creationWatcher = new ManagementEventWatcher { Query = new WqlEventQuery(creationQuery) };
            _creationWatcher.EventArrived += CreationWatcherEventArrived;

            var deletionQuery = string.Format("SELECT * FROM __InstanceDeletionEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_PnPEntity' AND TargetInstance.ClassGuid = '{0}'", PortGuid);
            _deletionWatcher = new ManagementEventWatcher { Query = new WqlEventQuery(deletionQuery) };
            _deletionWatcher.EventArrived += DeletionWatcherEventArrived;
        }

        private void CreationWatcherEventArrived(object sender, EventArrivedEventArgs e)
        {
            try
            {
                var serialPorts = SerialPort.GetPortNames();
                var port = serialPorts.FirstOrDefault(p => !_portList.Contains(p));
                if (!string.IsNullOrEmpty(port))
                {
                    _portList.Add(port);
                    var timeCreated = (ulong)e.NewEvent.Properties["TIME_CREATED"].Value;
                    var time = DateTime.FromFileTime((long)timeCreated);

                    OnAttached(new SerialPortModifiedEventArgs(port, time)); 
                }
            }
            catch (Exception ex)
            {
                //Logger.Current.Error(ex);
            }
        }

        private void DeletionWatcherEventArrived(object sender, EventArrivedEventArgs e)
        {
            try
            {
                var serialPorts = SerialPort.GetPortNames();
                var port = _portList.FirstOrDefault(p => !serialPorts.Contains(p));
                _portList.Remove(port);
                var timeCreated = (ulong)e.NewEvent.Properties["TIME_CREATED"].Value;
                var time = DateTime.FromFileTime((long)timeCreated);

                OnDetached(new SerialPortModifiedEventArgs(port, time));
            }
            catch (Exception ex)
            {
                //Logger.Current.Error(ex);
            }
        }

        public void Start()
        {
            try
            {
                var serialPorts = SerialPort.GetPortNames();

                _portList.AddRange(serialPorts);

                _creationWatcher.Start();
                _deletionWatcher.Start();
            }
            catch (Exception ex)
            {
                //Logger.Current.Error(ex);
            }
        }

        public void Stop()
        {
            _creationWatcher.Stop();
            _deletionWatcher.Stop(); 
        }

        public void Dispose()
        {
            _creationWatcher.Dispose();
            _deletionWatcher.Dispose();
        }


        public event EventHandler<SerialPortModifiedEventArgs> Attached;

        private void OnAttached(SerialPortModifiedEventArgs e)
        {
            var handler = Attached;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<SerialPortModifiedEventArgs> Detached;

        private void OnDetached(SerialPortModifiedEventArgs e)
        {
            var handler = Detached;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }


    public class SerialPortModifiedEventArgs : EventArgs
    {
        public SerialPortModifiedEventArgs(string port, DateTime time)
        {
            Port = port;
            Time = time;
        }

        public string Port { get; private set; }

        public DateTime Time { get; private set; }
    }
}
