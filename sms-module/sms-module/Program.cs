﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Vital;

namespace sms_module
{
    class Program
    {
        private static Dictionary<string, dynamic> Params = new Dictionary<string, dynamic>();



        static void Main(string[] args)
        {
            args = new string[] {
                "-com=COM18",
                "-to=+886978850143",
                "-message=Hello Lady GayGay, Mamama has sent you an invitation for Service/Repair. " +
                "Kindly complete your registration by following the instruction on this link: http://172.16.10.182:1337/visitor-registration/sd1t8adD6L",
                "-timeout=1000"
            };

            ConvertToKeyValue(args);

            var sendSMS = new Thread(SendSMS);
            sendSMS.SetApartmentState(ApartmentState.STA);
            sendSMS.Start();

            //var exportVideo = new Thread(ExportVideo);
            //exportVideo.SetApartmentState(ApartmentState.STA);
            //exportVideo.Start();

        }

        private static void SendSMS()
        {
            //throw new NotImplementedException();

            string comPort = GetParams("com");

            var sms = new SMSModem(comPort);

            sms.Open();

            Console.WriteLine("Port is OPEN");

            string recepient = GetParams("to");
            string message = GetParams("message");
            var response = sms.SendMessage(new SMSTextMessage(message, recepient));
            Console.WriteLine(response);
            sms.Close();       
            sms.Dispose();
            Console.ReadLine();


        }

        private static void ConvertToKeyValue(string[] args)
        {
            var rgx = new Regex("^-(?<key>[^=]+)=(?<value>[^=]+)", RegexOptions.IgnoreCase);
            foreach (var arg in args)
            {
                var match = rgx.Match(arg);
                Params.Add(match.Groups["key"].Value, match.Groups["value"].Value);
                //Console.WriteLine("{0}={1}", match.Groups["key"].Value, match.Groups["value"].Value);
            }
        }

        private static string GetParams(string key)
        {
            if (!Params.ContainsKey(key))
            {
                return String.Empty;
            }
            return Params[key];
        }


      
    }
}
