﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using SharpSMS;

namespace Vital
{
    public class SMSModem
    {
        private const string OK = "OK";
        private const int TIMEOUT = 30000;
        private const int BUFFER_SIZE = 16384;

        private readonly PNPSerialPortWatcher _serialPortWatcher;
        private SerialPort _serialPort;


        // Constructor
        public SMSModem(string portName, int baudRate = 115200, Parity parity = Parity.None, int dataBits = 8, StopBits stopBits = StopBits.One)
        {
            _serialPortWatcher = new PNPSerialPortWatcher(portName);
            _serialPortWatcher.Attached += SerialPortWatcherAttached;
            _serialPortWatcher.Detached += SerialPortWatcherDetached;
            _serialPortWatcher.Start();

            _serialPort = CreateSerialPort(portName, baudRate, parity, dataBits, stopBits);
        }


        // Property
        public bool IsOpen { get { return _serialPort.IsOpen; } }


        // Handlers
        private void SerialPortWatcherAttached(object sender, SerialPortModifiedEventArgs e)
        {
            //Logger.SMS.InfoFormat("{0} is attached.", e.Port);
            
            try
            {
                _serialPort = CreateSerialPort(_serialPort.PortName, _serialPort.BaudRate, _serialPort.Parity, _serialPort.DataBits, _serialPort.StopBits);
            }
            catch (Exception ex)
            {
                //Logger.SMS.Error(ex);
            }

            OnConnected(new ConnectionChangedEventArgs(e.Time));
        }

        private void SerialPortWatcherDetached(object sender, SerialPortModifiedEventArgs e)
        {
            //Logger.SMS.InfoFormat("{0} is detached.", e.Port);

            try
            {
                if (IsOpen)
                {
                    // NOTE: It must release the resources after disconnected, 
                    //       otherwise it throws an exception could cause the application crash.
                    _serialPort.Dispose();
                }
            }
            catch (Exception ex)
            {
                //Logger.SMS.Error(ex);
            }

            OnDisconnected(new ConnectionChangedEventArgs(e.Time));
        }


        // Methods
        private static SerialPort CreateSerialPort(string portName, int baudRate = 115200, Parity parity = Parity.None,
            int dataBits = 8, StopBits stopBits = StopBits.One)
        {
            var serialPort = new SerialPort(portName, baudRate, parity, dataBits, stopBits)
            {
                Encoding = Encoding.GetEncoding("iso-8859-1"),
                DtrEnable = true,
                DiscardNull = true,
                ReadBufferSize = BUFFER_SIZE,
                WriteBufferSize = BUFFER_SIZE,
                ReadTimeout = TIMEOUT,
                WriteTimeout = TIMEOUT,
                Handshake = Handshake.RequestToSend,
                ReceivedBytesThreshold = 1
            };

            return serialPort;
        }

        public string SendMessage(ISMSMessage message)
        {
            //SendCommand("AT+CMGF=0");// "//string.Format("AT+CMGF=0", (int)message.Format));

            //SendCommand("AT+CMGS=153");
            //Thread.Sleep(200);
            ////var response = SendCommand("0011000C918896875810340000A70AC8329BFDBEBEE56C32" + "\r" + char.ConvertFromUtf32(26) + "\r");
            ////var response = SendCommand(char.ConvertFromUtf32(26));

            ////// To request to send the message.
            ////SendCommand(string.Format("AT+CMGS=153", message.GetSendData()));

            ////// Send the message 
            //SendCommand("0041000C918896875810340011A006080400010201C8329BFD0631C3E43CE818CE1FC37916A8196E87DB61103" +
            //    "A3C07CDCB6E3A28FFAE83C26E50DA6D4FD3C3F4F4DB0D32BFE5A069596E4F8FCBA017485A8687D372D732ED26B3F3A0F1BB0D6797E9" +
            //    "6550FE5D9783E4E5737A4E9787E9E9B71B24CE83CC6F36FB7D4FBBCF203ABA0C4ABBE774797D4C4FBFDDA0B71B4447A7E72076DABDD681D0"
            //    + "\r" + char.ConvertFromUtf32(26) + "\r");
            ////SendCommand(char.ConvertFromUtf32(26));

            //////SendCommand("^z");

            //SendCommand("AT+CMGS=69");

            ////// Send the message 
            //var response =  SendCommand("0041000C9188968758103400114006080400010202743A5CF77AC56E3257CCE68AC15C319C4C179BCD6E2F7B7A9EA6BFE52D79F99" +
            //    "C9ED3E5617AFAED7ECDC9313A2E4C26DA98" + "\r" + char.ConvertFromUtf32(26) + "\r");
            ////var response = SendCommand(char.ConvertFromUtf32(26));



            //Logger.SMS.InfoFormat("Send Message: {2}\r\nTo: {0}\t, {1}", message.To, message.Content, response);


            /// <------------------------- Real Part ------------------------------> ///
            /// 

            SendCommand("AT+CMGF=0");
            var response = "";
            TextMessage textMessage = new TextMessage();

            textMessage.Text = message.Content;
            SMSSubmit sms = new SMSSubmit();
            sms.PhoneNumber = message.To;
            sms.MessageToSend = textMessage;

            List<byte[]> messageList = sms.GetPDUList();

            foreach (byte[] messageBytes in messageList)
            {
                int messageLength = messageBytes.Length - 1;
                string pduMessage = BytesToHexString(messageBytes);
                string txtCMGSpart1 = string.Format("AT+CMGS={0}", messageLength);
                Console.WriteLine(txtCMGSpart1);
                response += SendCommand(txtCMGSpart1);
                Console.WriteLine(pduMessage);
                response += SendCommand(pduMessage +"\r" + char.ConvertFromUtf32(26) + "\r");

                Thread.Sleep(500);
            }
            return response;
        }

        public string SendCommand(string command)
        {
            return Send(command + "\r");
        }

        private string Send(string commnad)
        {
            //Console.WriteLine(commnad);
            if (!_serialPort.IsOpen) throw new InvalidOperationException(string.Format("{0} is not opened.", _serialPort.PortName));

            //Logger.SMS.DebugFormat("Send command: {0}", commnad);

            _serialPort.DiscardInBuffer();
            _serialPort.DiscardOutBuffer();
            _serialPort.Write(commnad);

            string result = null;
            var buffer = new StringBuilder();
            var sw = new Stopwatch();
            sw.Start();
            while (sw.ElapsedMilliseconds < TIMEOUT)
            {
                if (_serialPort.BytesToRead > 0)
                {
                    var data = _serialPort.ReadExisting();
                    buffer.Append(data);

                    result = buffer.ToString();

                    Console.WriteLine(result);
                    if (EndOfStream(result))
                    {
                        return result;
                    }
                }
                Thread.Sleep(20);
            }

            if (!EndOfStream(result))
            {
                //throw new Exception(string.Format("Sending command ({0}) timeout.", commnad));
                //return string.Format("Sending command ({0}) timeout.", commnad);
            }

            return result;
        }

        private bool EndOfStream(string data)
        {
            if (string.IsNullOrEmpty(data)) return false;

            return data.Contains(OK) || data.Contains("ERROR") || data.Contains(">");
        }

        private void Initialize()
        {
            try
            {
                var response = SendCommand("ATE0");
                if (response.Contains(OK))
                {
                    //Logger.SMS.Info("ATE0: OK");
                    Console.WriteLine("ATE0: OK");
                }
                else
                {
                    //Logger.SMS.ErrorFormat("ATE0: {0}", response);
                    Console.WriteLine("ATE0: {0}", response);
                }

                // Restore to default settings.
                response = SendCommand("ATZ");
                if (response.Contains(OK))
                {
                    //Logger.SMS.Info("ATZ: OK");
                    Console.WriteLine("ATZ: OK");
                }
                else
                {
                    //Logger.SMS.ErrorFormat("ATZ: {0}", response);
                    Console.WriteLine("ATZ: {0}", response);
                }

                // Signal quality
                response = SendCommand("AT+CSQ");

                //Logger.SMS.InfoFormat("Signal: {0}", response);
                Console.WriteLine("Signal: {0}", response);
            }
            catch (Exception ex)
            {
                //Logger.SMS.Error(ex);
                Console.WriteLine(ex.Message);
            }
        }

        public void Open()
        {
            if (_serialPort.IsOpen) return;

            var ports = SerialPort.GetPortNames();

            if (!ports.Contains(_serialPort.PortName)) return;

            _serialPort.Open();

          //  Initialize();
        }

        public void Close()
        {
            if (!_serialPort.IsOpen) return;

            _serialPort.Close();
        }

        public void Dispose()
        {
            _serialPortWatcher.Attached -= SerialPortWatcherAttached;
            _serialPortWatcher.Detached -= SerialPortWatcherDetached;
            _serialPortWatcher.Stop();
            _serialPortWatcher.Dispose();

            _serialPort.Dispose();
        }


        // Event
        public event EventHandler<ConnectionChangedEventArgs> Connected;

        private void OnConnected(ConnectionChangedEventArgs e)
        {
            var handler = Connected;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<ConnectionChangedEventArgs> Disconnected;

        private void OnDisconnected(ConnectionChangedEventArgs e)
        {
            var handler = Disconnected;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        private static string BytesToHexString(byte[] srcArray)
        {
            string retString = string.Empty;
            foreach (byte b in srcArray)
                retString += b.ToString("X2");

            return retString;
        }
    }
}
