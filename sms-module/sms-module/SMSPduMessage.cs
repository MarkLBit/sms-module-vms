using System;
using System.Text;
using Constant.Utility;

namespace Vital
{
    public class SMSPduMessage : ISMSMessage
    {
        private readonly string _pdu;


        public SMSPduMessage(string content, string to)
        {
            Content = content;
            To = to;

            var data = Encoding.BigEndianUnicode.GetBytes(Content);

            var da = "81";          // National
            var receiver = To;
            if (To.StartsWith("+"))
            {
                da = "91";          // International
                receiver = To.TrimStart('+');
            }

            var sb = new StringBuilder();
            sb.Append("00");
            sb.Append("31");        // Status Report (Yes: 31, No: 11)
            sb.Append("00");        // MR
            sb.Append(receiver.Length.ToString("X2"));
            sb.Append(da);
            sb.Append(SemiOctetToString(receiver));
            sb.Append("00");        // PID
            sb.Append("08");        // DCS: 16 bits
            sb.Append("A8");        // VP: validity period
            sb.Append(data.Length.ToString("X2"));
            sb.Append(data.ToHexString());

            _pdu = sb.ToString();
        }


        public MessageFormat Format { get { return MessageFormat.Pdu; } }
        public string To { get; set; }
        public string Content { get; set; }


        public string GetSendData()
        {
            return (_pdu.Length / 2 - 1).ToString();
        }

        public string ToMessage()
        {
            // (char)26: crtl + z
            return _pdu + char.ConvertFromUtf32(26); ;
        }

        private static string SemiOctetToString(string inp) //sp
        {
            if (inp.Length % 2 == 1)
            {
                inp = inp + "F";
            }

            var output = new StringBuilder();
            for (var i = 0; i < inp.Length; i = i + 2)
            {
                var temp = inp.Substring(i, 2);
                output.Append(temp[1]);
                output.Append(temp[0]);
            }

            return output.ToString();
        }
    }
}